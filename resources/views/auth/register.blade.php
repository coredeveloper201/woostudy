@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-4">
                      <img src='{{ asset('images/static/logo.png') }}' class="img-responsive" style="height: 60px" alt=''/>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Who are you') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name='role_id' id='role' class="form-control">
                                    @foreach ($roles as $role)
                                        <option value='{{$role->id}}'>{{$role->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('role_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                          <div class='col-md-8 mx-auto'>
                            <label class="d-flex justify-content-center align-items-center ">
                              <input id="terms_and_condition" class="mx-3" type='checkbox' />
                              <span>I agree to Terms and conditions. That any information may result in the cancellation of admissions, or you can be block listed to the all our affiliate sites</span>
                            </label>
                          </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="registration_submit_button" type="submit" disabled class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4 social-connect">
                            <!--Facebook-->
                            <button type="button" class="btn btn-fb"><i class="fa fa-facebook pr-1"></i>
                                Facebook
                            </button>
                            <!--Twitter-->
                            <button type="button" class="btn btn-tw"><i class="fa fa-twitter pr-1"></i> Twitter
                            </button>
                            <!--Google +-->
                            <button type="button" class="btn btn-gplus"><i class="fa fa-google-plus pr-1"></i>
                                Google +
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css_stack')
    <style>
        .social-connect{
            padding-top: 20px;
        }
    </style>
@endpush
@push('js_stack')
<script>
$(function () {
  var $rsb = $('#registration_submit_button')
  var $tac = $('#terms_and_condition')
  $tac.on('click', function () {
    if ($(this).is(':checked')) {
      $rsb.removeAttr('disabled')
      console.log('remove attr')
    }else {
      $rsb.attr('disabled', true);
      console.log('add attr')
    }
  })
})
</script>
@endpush
