@extends('layouts.app')
@section('content')
<div class="container">
  <h3>
    You are logged in as {{auth()->user()->getRole()}}
  </h3>
</div>
@endsection
