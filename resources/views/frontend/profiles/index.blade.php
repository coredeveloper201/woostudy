@extends('layouts.app')
@section('content')
<app-profile user_id="{{$user->id}}"></app-profile>
@endsection

@push('js_stack')
<script src='{{ asset('js/profilepage.js') }}'></script>
<script>
$(function () {
  $('#sidebar_metis_menu').metisMenu();
})
</script>
@endpush




