export default {
  methods: {
    getRandomNumber(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }
  }
}
