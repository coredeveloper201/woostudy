<?php

Route::group(['prefix' => 'json'], function () {


/**
 * Profile page
 */

  Route::get('get_auth_user_data', 'ProfileController@get_auth_user_data');

  //personal info
  Route::get('get_profile_personal_info/{profile_id}', 'ProfileController@get_profile_personal_info');
  Route::post('post_profile_personal_info', 'ProfileController@post_profile_personal_info');


  //contact info
  Route::get('get_contact_details/{profile_id}', 'ProfileController@get_contact_details');
  Route::post('post_contact_details', 'ProfileController@post_contact_details');


  // social media url
  Route::get('get_social_media_details/{profile_id}', 'ProfileController@get_social_media_details');
  Route::post('post_social_media_details', 'ProfileController@post_social_media_details');

 // // education details
 //  Route::get('get_education_details', 'ProfileController@get_education_details');
 //  Route::post('post_education_details', 'ProfileController@post_education_details');
 //  Route::delete('delete_education_item/{id}', 'ProfileController@delete_education_item');


 // education details
  Route::get('get_education_details/{profile_id}', 'ProfileController@get_education_details');
  Route::post('post_education_details', 'ProfileController@post_education_details');
  Route::delete('delete_education_item/{id}', 'ProfileController@delete_education_item');
  Route::put('update_education_item/{id}', 'ProfileController@update_education_item');


 // ielts or tofel details
  Route::get('get_ielts_details/{profile_id}', 'ProfileController@get_ielts_details');
  Route::post('post_ielts_details', 'ProfileController@post_ielts_details');
  Route::delete('delete_ielts_item/{id}', 'ProfileController@delete_ielts_item');
  Route::put('update_ielts_item/{id}', 'ProfileController@update_ielts_item');


 // interest
  Route::get('get_interest_details/{profile_id}', 'ProfileController@get_interest_details');
  Route::post('post_interest_details', 'ProfileController@post_interest_details');
  Route::delete('delete_interest_item/{id}', 'ProfileController@delete_interest_item');
  Route::put('update_interest_item/{id}', 'ProfileController@update_interest_item');


  // video upload
  Route::post('post_video_details', 'ProfileController@post_video_details');
  Route::get('get_video_details/{profile_id}', 'ProfileController@get_video_details');
  Route::delete('delete_video_item/{id}', 'ProfileController@delete_video_item');



});

Route::resource('test', 'TestController');
