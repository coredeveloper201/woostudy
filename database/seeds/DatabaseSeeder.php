<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(ProfileOptionsTableSeeder::class);
        $this->call(IeltsTableSeeder::class);
        $this->call(InterestsTableSeeder::class);
    }
}
