<?php

use App\Ielts;
use Faker\Factory;
use Illuminate\Database\Seeder;

class IeltsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Ielts::truncate();
      $faker = Factory::create();
      foreach (range(0, 15) as $key) {
        Ielts::create([
          "exam" => $faker->word,
          "user_id" => rand(1, 4),
          "score" => rand(20, 1000),
          "taken_on" => $faker->date($format = 'Y-m-d', $max = 'now'),
          "grade" => ['a', 'b', 'c', 'd', 'e'][rand(0, 4)],
        ]);
      }

    }
}
