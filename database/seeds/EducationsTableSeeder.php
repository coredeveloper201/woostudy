<?php

use App\Education;
use Illuminate\Database\Seeder;
use Faker\Factory;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      Education::truncate();
      $faker = Factory::create();
      foreach (range(0, 15) as $key) {
        Education::create([
          "grade_name" => $faker->name,
          "user_id" => rand(1, 4),
          "grade" => $faker->word,
          "institute" => $faker->word,
          "completion_date" => $faker->date($format = 'Y-m-d', $max = 'now'),
          "percentage_obtained" => rand(50, 90),
          "education_level" => $faker->word,
        ]);
      }
    }
}
