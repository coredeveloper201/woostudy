<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'medias', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'mediable_id' );
            $table->string( 'mediable_type' );
            $table->string( 'title' )->nullable();
            $table->string( 'filename' );
            $table->string( 'mime_type' );
            $table->string( 'ext' );
            $table->string( 'size' );
            $table->string( 'original_filename' );
            $table->string( 'status' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'medias' );
    }
}
