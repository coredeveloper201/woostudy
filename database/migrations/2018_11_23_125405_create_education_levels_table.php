<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'education_levels', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->increments( 'title' )->unique();
            $table->boolean( 'status' )->default( false );
            $table->unsignedInteger( 'created_by' );
            $table->unsignedInteger( 'approved_by' )->nullable();
            $table->timestamp( 'approved_at' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'education_levels' );
    }
}
