<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIeltsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'ielts', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'ielts_exam_level_id' );
            $table->unsignedInteger( 'user_id' );
            $table->string( 'score' )->nullable();
            $table->date( 'taken_on' )->nullable();
            $table->string( 'grade' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'ielts' );
    }
}
