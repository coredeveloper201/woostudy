# technology stack

* php7.2
* Mysql 5.7 
* laravel 5.7 
* Vue 2.5 and VueX
* mongodb or any other NoSql database for chat communication (later)

# this week task
complete authentication, password resets for teacher, students
complete role based login registration system
setting up teacher students profile page
connection between teacher, students


# table structure for state above task
common fields: id, created_at, updated_at
# users 
  * all_default_columns++
  * role_id
roles
  * id
  * name
  * slug
profile
  * id
  * profile information related all columns++

# relationship
user has one profile 
use has one role

roles has many users 
profile belongsTo User


I asked to work 5 hours in a day, later I will work 4h in a day


# split task in to 7 days 

day one + day two 
==================
html css mockup for login, registration, homepage boiler code, profile page, set up boiler plate code

day three 
==========
complete role based login, registration

day four
=======
working on profile page teacher, students, school

day five
=======
working on profile page teacher, students, school

day six
=======
working on profile page teacher, students, school

day seven
=========
connection between teacher, students, school. use laravel-friendships for connection











